class Num(object):
    def __init__(self, n):
        self.n = n

class Add(object):
    def __init__(self, left, right):
        self.left = left
        self.right = right

class Sub(object):
    def __init__(self, left, right):
        self.left = left
        self.right = right

class Mul(object):
    def __init__(self, left, right):
        self.left = left
        self.right = right

class Div(object):
    def __init__(self, left, right):
        self.left = left
        self.right = right

class Id(object):
    def __init__(self, name):
        self.name = name

class Assign(object):
    def __init__(self, var, val):
        self.var = var
        self.val = val

class Fun(object):
    def __init__(self, params, body):
        self.params = params  
        self.body = body

class App(object):
    def __init__(self, fun, args):
        self.fun = fun
        self.args = args
