import re
from model import *

""" parsing order is related to the order of evaluation """
def parse(string):

    #assignment
    m = re.search('^(.+)\s*=\s*(.+)$', string)
    if m:
        variable = m.group(1)
        value = m.group(2) 
        print('assign: {} = {}'.format(variable, value))
        return Assign(variable, parse(value)) 

    #addition
    m = re.search('([^\(]|^)(.+)\s*\+\s*(.+)([^\)]|$)', string)
    if m:
        left = m.group(1)
        right = m.group(2)
        print('add: {} + {}'.format(left, right))
        return Add(parse(left), parse(right))

    #subtraction
    m = re.search('[^\(](\w+)\s*\-\s*(\w+)[^\)]', string)
    if m:
        left = m.group(1)
        right = m.group(2)
        print('sub: {} - {}'.format(left, right))
        return Sub(parse(left), parse(right))

    #multiplication
    m = re.search('[^\(](\w+)\s*\*\s*(\w+)[^\)]', string)
    if m:
        left = m.group(1)
        right = m.group(2)
        print('multiple: {} * {}'.format(left, right))
        return Mul(parse(left), parse(right))

    #division
    m = re.search('[^\(](\w+)\s*\/\s*(\w+)[^\)]', string)
    if m:
        left = m.group(1)
        right = m.group(2)
        print('divide: {} / {}'.format(left, right))
        return Div(parse(left), parse(right))

    #bracket
    m = re.search('^\((.+)\)$', string)
    if m:
        content = m.group(1)
        print('bracket: ({})'.format(content))
        return parse(content)

    #number
    m = re.search('[^a-z]([0-9]+)[^a-z]', string)
    if m:
        num = m.group(1)
        print('num: {}'.format(num))
        return Num(num)

    #identifier
    m = re.search('^(\w)$', string)
    if m:
        name = m.group(1)
        print('name: {}'.format(name))
        return Id(name)
    
    raise Exception()





